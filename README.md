# Mega-Structures-Requirements
Stellaris Mod Mega Structures Requirements

###  *Supported Version: 1.6.** ###
##  Description: ##
Alters the game so the current Megastructures can be both built and upgraded at the same time.
Also adds the ability to build multiple Megastructures per system(Ringworld and Sphere have to be build first and can't be in the same System). Currently not working will be included in next release.

## Features: ##
Build Multiple Megastructures at a time (includes Ruined Megastructures).
Build Multiple per system (ie Ring World and Science Nexus)

## Incompatibilities: ##
Anything that alters the Default Megastructures themselves.
The Files for these are as follows:
00_ring_world
01_dyson_sphere
02_spy_orb
03_think_tank
06_matter_decompressor
07_strategic_coordination_center
08_mega_art_installation
09_interstellar_assembly

---

#### Support: ####
*https://jira.meridanas.me/servicedesk/customer/portal/5*

#### Direct Download: ####
*https://bitbucket.org/meridanas/mega-structures-requirements/downloads/*